# SAGE: Sistema Auxiliar de Gerenciamento de Estadias

Responsáveis:
 - Andreas Ramalho Spyridakis;
 - Davi Delgado Torres;
 - Éric de Barcelos Moreira;
 - Lucas Bianchi de Araújo;
 - Lucas Oliveira Marques da Rocha; e
 - Marina Ribeiro Kehagias.

## Apresentação

Projeto da disciplina de engenharia de software do 1o semestre de 2021
orientado pelo professor Glauber Boff do Centro Universitário de Brasília
(UniCEUB).

O projeto consiste em desenvolver um programa que atenda algumas necessidades
gerenciais diárias de hospedarias de pequeno e médio porte.
